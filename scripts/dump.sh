#!/bin/bash
# support two arguments:
#   *config_file*   - file with some variables, required
#   -v              - is set, then print logs, not required, by default is false

CONFIG_FILE="$1"
VERBOSE="false"
if [ "$2" = "-v" ]; then
  VERBOSE="true"
fi

# contains vars SERVER, USER_NAME, USER_PASS, MAX_DUMP_COUNT, MEM_LIMIT_USAGE and MEM_LIMIT_FREE
if ! source "$CONFIG_FILE"; then
  echo "invalid config file: $CONFIG_FILE" >&2
  exit 1
fi


SERVER_URI="http://$SERVER"


CUR_DATE=$(date --rfc-3339=seconds)
SERVER_FOLDER="/archives/$SERVER"
SAVE_FOLDER="$SERVER_FOLDER/$CUR_DATE"


# temporary directory for dumping
TMP_DIR="/tmp/dump_dir"

# recreate temporary directory
rm $TMP_DIR -rf
mkdir $TMP_DIR -p


LUA_EXE="luajit"
source "/lua-server-api/api/source.sh"


function log_info() {
  msg=$1
  echo "INF: $msg - $SERVER - $CUR_DATE"
}

function log_warning() {
  msg=$1
  echo "WRN: $msg - $SERVER - $CUR_DATE" >&2
}

function log_error() {
  msg=$1
  echo "ERR: $msg - $SERVER - $CUR_DATE" >&2
}


function get_token() {
  login=$1
  password=$2

  token=$($LUA_EXE -e "
  -- switch off printing logs
  local auxilary = require('api.auxilary')
  auxilary.log = function()end

  local user_api = require('api.user')
  local user = user_api.new('$SERVER_URI'):unsafe()

  local token = user:get_token('$login', '$password', 60*60)
  print(token)
  ")

  if [ $? -ne 0 ]; then
    return 1
  fi

  echo "$token"
}

function dump_pool() {
  token=$1
  database=$2
  collection=$3
  folder=$4

  archive_name="$database@$collection.gz"
  archive="$folder/$archive_name"

  log_info "dumping $archive_name"

  $LUA_EXE -e "
  if ($VERBOSE == false) then
    -- switch off printing logs
    local auxilary = require('api.auxilary')
    auxilary.log = function()end
  end

  local misc_api = require('api.misc')
  local misc = misc_api.new('$SERVER_URI'):unsafe()

  misc:download_archive('$token', '$archive', '$database', '$collection')
  "

  if [ $? -ne 0 ]; then
    log_error "can't download archive $archive_name"
    rm "$archive" -f
    exit 1
  fi
}


# return count of dumps for removing
function dump_count_for_remove() {
  folder=$1
  max_count=$2

  cur_count=$(ls "$folder" | wc -l)
  if (( cur_count <= max_count )); then
    echo 0
    return 0
  fi

  echo $(( cur_count - max_count ))
}

function remove_oldest_dumps() {
  folder=$1
  count_for_removing=$2

  for (( i=0; i<count_for_removing; ++i )); do
    oldest=""
    time=$(date +%s)
    for dump_name in "$folder"/*; do
      dump_date=${dump_name##*/}
      dump_time=$(date +%s -d "$dump_date")
      if (( dump_time < time )); then
        oldest=$dump_name
        time=$dump_time
      fi
    done

    if ! rm "$oldest" -r; then
      log_error "can't remove dump: $oldest"
      return 1
    fi

    log_info "remove oldest dump: $oldest"
    return 0
  done
}

function check_mem_usage() {
  folder=$1
  limit=$2

  current_mem_usage=$(du -sh "$folder" --block-size=G | awk '{print $1}' | grep -E -io "[0-9]+")

  log_info "current memory usage: $current_mem_usage"
  if (( current_mem_usage > limit )); then
    log_warning "current memory usage is bigger than limit: ${current_mem_usage}G > ${limit}G"
  fi
}

function check_mem_free() {
  folder=$1
  limit=$2

  current_mem_free=$(df -h "$folder" --block-size=G | tail -1 | awk '{print $4}' | grep -E -io "[0-9]+")

  log_info "current free memory: $current_mem_free"
  if (( current_mem_free < limit )); then
    log_warning "current free memory is less than limit: ${current_mem_free}G > ${limit}G"
  fi
}


# return list of md5sum (as string)
function get_md5_of_dir() {
  dir=$1

  md5list=()
  for file in "$dir"/*; do
    file_md5=("$(md5sum "$file")")
    md5list+=("${file_md5[0]}")
  done

  echo "${md5list[*]}"
}

# return latest dump in server folder
function get_latest_dump() {
  server_folder=$1

  latest=""
  time=0
  for dump_file in "$server_folder"/*; do
    dump_date=${dump_file##*/}
    dump_time=$(date +%s -d "$dump_date")
    if (( dump_time > time )); then
      latest=$dump_file
      time=$dump_time
    fi
  done

  echo "$latest"
}

# return 1 if the dump is dublicate, 0 - otherwise
function is_dublicate() {
  cur_dump=$1
  server_folder=$2

  latest_dump=$(get_latest_dump "$server_folder")
  # if empty then current dump is not a dublicate
  if [ "$latest_dump" = "" ]; then
    echo 0
    return 0
  fi

  lastest_md5=$(get_md5_of_dir "$latest_dump")
  current_md5=$(get_md5_of_dir "$cur_dump")

  if [ "$lastest_md5" = "$current_md5" ]; then
    echo 1
    return 0
  fi

  echo 0
}


# dumping
log_info "start dumping"


TOKEN=$(get_token "$USER_NAME" "$USER_PASS")
if [ $? -ne 0 ]; then
  log_error "fail while getting token for server"
  exit 1
fi


# TODO check exit codes for every call
dump_pool   "$TOKEN" "sp_user"    "users"     "$TMP_DIR"

dump_pool   "$TOKEN" "sp_assets"  "tags"      "$TMP_DIR"
dump_pool   "$TOKEN" "sp_assets"  "previews"  "$TMP_DIR"
dump_pool   "$TOKEN" "sp_assets"  "assets"    "$TMP_DIR"

dump_pool   "$TOKEN" "sp_content" "source"    "$TMP_DIR"
dump_pool   "$TOKEN" "sp_content" "group"     "$TMP_DIR"


# NOTE: while we dump user databse we always have unique dumps, because tokens stores in user database
log_info "check dublicates"
if [ "$(is_dublicate "$TMP_DIR" "$SERVER_FOLDER")" -ne 0 ]; then
  log_info "current dump is a dublicate of latest dump, so does nothing"
  rm "$TMP_DIR" -rf
  exit 0
fi

log_info "move temorary dump to server dump folder"
mv "$TMP_DIR" "$SAVE_FOLDER"


# remove oldest dumps
for_remove_count=$(dump_count_for_remove "$SERVER_FOLDER" "$MAX_DUMP_COUNT")
if ! remove_oldest_dumps "$SERVER_FOLDER" "$for_remove_count"; then
  log_error "error while removing old dumps"
  exit 1
fi


# check memory limits
check_mem_usage "$SERVER_FOLDER" "$MEM_LIMIT_USAGE"
check_mem_free  "$SERVER_FOLDER" "$MEM_LIMIT_FREE"


log_info "dumping complete"
