# w2w-backend dumper

## deploy

1. Copy [dumper.yaml.tmplt](dumper.yaml.tmplt) to [dumper.yaml](dumper.yaml)

2. Set path to lua server api in [dumper.yaml](dumper.yaml) in string:

```yaml
      - "PATH_TO_LUA_SERVER_API:/lua-server-api/api"
```

3. Set needed settings in [dumper.yaml](dumper.yaml) in string:

```bash
...
        printf "SERVER=my_server.org\nUSER_NAME=admin\nUSER_PASS=admin\nMAX_DUMP_COUNT=5\nMEM_LIMIT=20" > /config
...
```

4. Set time intervals in [dumper.yaml](dumper.yaml) in string:


```bash
        printf "SHELL=/bin/bash\n0 12 * * * /scripts/dump.sh &>> /var/log/dump.log\n" > /etc/cron.d/dump
```

5. Run [deploy_dumper.sh](deploy_dumper.sh)
